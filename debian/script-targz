#!/bin/bash

# Copyright: 2018 Dmitry Smirnov <onlyjob@member.fsf.org>
#            2021 Sophie Brun <sophie@offensive-security.com>
# License: GPL-3+
# This program is free software: you can redistribute it and/or modify  it
# under the terms of the GNU General Public License as published by  the
# Free Software Foundation, either version 3 of the License, or  (at your
# option) any later version.  .  This package is distributed in the hope
# that it will be useful,  but WITHOUT ANY WARRANTY; without even the
# implied warranty of  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the  GNU General Public License for more details.  .  You should have
# received a copy of the GNU General Public License  along with this
# program. If not, see <http://www.gnu.org/licenses/>.

set -e

if [ "$1" = '--upstream-version' ]; then
    new_version="$2"
else
    printf "Missing --upstream-version argument" 1>&2
    exit 1
fi

if echo $new_version | grep beta; then
    upstream_version=$(echo $new_version | sed -e "s/~beta/-beta/")
else
    upstream_version=$new_version
fi

DEB_SOURCE="$(dpkg-parsechangelog -SSource)"

filename="$(readlink -f ../${DEB_SOURCE}_${new_version}.orig.tar.gz)"
current_dir=$(pwd)
echo ${current_dir}

work_dir="$(mktemp -d -t get-orig-source_${DEB_SOURCE}_${new_version}_XXXXXXX)"
trap "rm -rf '$work_dir'" EXIT

git clone --quiet --branch v$upstream_version --depth=1 --recurse-submodules --shallow-submodules https://github.com/vlaci/pyperscan "$work_dir"

tar --exclude=.git* -zcvf ../pyperscan_$new_version.orig.tar.gz -C $work_dir .

rm -rf $work_dir

